# Module that implements a Q-Learner for the Mountain Car Problem.

import numpy as np

# Mínimo de incremento de aprendizaje. Vamos aprendiendo, mientras el
# incremento de aprendizaje sea superior a dicho valor.
EPSILON_MIN = 0.005

# Razón de aprendizaje del agente 
ALPHA = 0.05

# Factor de descuento del agente
GAMMA = 0.98

# Número de divisiones en el caso de discretizar el espacio de estados continuo. 
NUM_DISCRETE_BINS = 30

# Número máximo de pasos a realizar en cada episodio, como meta.
STEPS_PER_EPISODE = 200

# Número máximo de iteraciones que estamos dispuestos a realizar.
MAX_NUM_EPISODES = 10000

max_num_steps = MAX_NUM_EPISODES * STEPS_PER_EPISODE
EPSILON_DECAY = 500 * EPSILON_MIN / max_num_steps


class QLearner(object):
    def __init__(self, environment):
        self.obs_shape = environment.observation_space.shape
        self.obs_high = environment.observation_space.high
        self.obs_low = environment.observation_space.low
        print(f'Shape: {self.obs_shape}')
        print(f'High: {self.obs_high}')
        print(f'Low: {self.obs_low}')
        self.obs_bins = NUM_DISCRETE_BINS
        self.bin_width = (self.obs_high-self.obs_low)/self.obs_bins
        self.action_shape = environment.action_space.n
        self.Q = np.zeros((self.obs_bins+1, self.obs_bins+1, self.action_shape))
        self.alpha = ALPHA
        self.gamma = GAMMA
        self.epsilon = 1.0
        
    def discretize(self, obs):
        if type(obs) is tuple:
            obs = obs[0]
        return tuple(((obs-self.obs_low)/self.bin_width).astype(int))
        
    def get_action(self, obs):
        discrete_obs = self.discretize(obs)
        # Selección de la acción en base a Epsilon-Greedy
        if self.epsilon > EPSILON_MIN:
            self.epsilon -= EPSILON_DECAY
        # #Con probabilidad 1-epsilon, elegimos la mejor posible
        if np.random.random() > self.epsilon: 
            return np.argmax(self.Q[discrete_obs])
        else:
            return np.random.choice([a for a in range(self.action_shape)])#Con probabilidad epsilon, elegimos una al azar
        
    def learn(self, obs, action, reward, next_obs):
        discrete_obs = self.discretize(obs)
        discrete_next_obs = self.discretize(next_obs)
        self.Q[discrete_obs][action] += self.alpha*(reward + self.gamma * np.max(self.Q[discrete_next_obs]) - self.Q[discrete_obs][action])
        
