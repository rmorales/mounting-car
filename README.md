# Aprendizaje por reforzamiento para el carrito de montaña

Este proyecto incluye un cuaderno para observar un agente con comportamiento aleatorio y un agente con aprendizaje por reforzamiento (Q-Learning) intentanto resolver el problema de Carrito de Montaña.

El proyecto usa [`gymnasium`](https://github.com/Farama-Foundation/Gymnasium), la versión de mantenimiento de lo que fue OpenAI Gym.

El proyecto se ha probado usando Linux 22.04 y la versión 3-2023.07-2 de Anaconda (distribución de Python). Se incluyen los archivos `environment.yml` y `requirements.txt` para reproducir el ambient de ejecución.

